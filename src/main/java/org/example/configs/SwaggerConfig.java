package org.example.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import javax.servlet.ServletContext;

//import io.swagger.v3.oas.annotations.OpenAPIDefinition;
//import io.swagger.v3.oas.annotations.info.Contact;
//import io.swagger.v3.oas.annotations.info.Info;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class SwaggerConfig {
//
//    @OpenAPIDefinition(
//            info = @Info(
//                    title = "Loyalty System Api",
//                    description = "Loyalty System", version = "1.0.0",
//                    contact = @Contact(
//                            name = "Struchkov Mark",
//                            email = "mark@struchkov.dev",
//                            url = "https://mark.struchkov.dev"
//                    )
//            )
//    )
//    public class OpenApiConfig{
//
//    }
//}
@Configuration
public class SwaggerConfig {


    public SwaggerConfig() {
    }

    @Bean
    public Docket productApi(ServletContext context) {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .useDefaultResponseMessages(false)
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.example"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Citizen API")
                .description("Позволяет производить простейшие операции над базой данных граждан")
                .version("0.1")
                .build();
    }

}
