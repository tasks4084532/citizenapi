package org.example.controllers;

import io.swagger.annotations.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.entity.Citizen;
import org.example.exceptions.CitizenNotFoundException;
import org.example.repos.CitizenRepo;
import org.example.responses.UserPostResponse;
import org.example.service.CitizenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Min;
import java.util.List;

@Slf4j
@RestController
@Api(tags = "Controller")
@Validated
@AllArgsConstructor
public class Controller {
    private final CitizenService citizenService;

    @Autowired
    CitizenRepo citizenRepo;

    @GetMapping(path = "/hello-world")
    @ApiOperation(value = "Тестовый метод", hidden = true)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Формирование прошло успешно"),
            @ApiResponse(code = 400, message = "Ошибка запроса"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервиса")
    }

    )
    public String helloWorld(
            @ApiParam(name = "number", value = "Загаданное число", example = "1", required = true)
            @RequestParam("number") @Min(0) int number
    ) {
        log.info("hello_world called");
        return this.citizenService.helloWorld(number);
    }

    @GetMapping(path = "/citizens/{first_name, last_name, middle_name, birth_date}")
    @ApiOperation(value = "Возвращает информацию о гражданине по его id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Поиск прошёл успешно"),
            @ApiResponse(code = 400, message = "Ошибка запроса"),
            @ApiResponse(code = 404, message = "Гражданин не найден"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервиса")
    }
    )
    public ResponseEntity<List<Citizen>> getCitizens(
            @ApiParam(name = "first_name", value = "Имя", example = "Михаил")
            @RequestParam(value = "first_name", required = false)
            String firstName,
            @ApiParam(name = "last_name", value = "Фамилия", example = "Горшенёв")
            @RequestParam(value = "last_name", required = false)
            String lastName,
            @ApiParam(name = "middle_name", value = "Отчество", example = "Юрьевич")
            @RequestParam(value = "middle_name", required = false)
            String middleName,
            @ApiParam(name = "birth_date", value = "Дата рождения в формате dd.MM.yyyy", example = "19.06.2013")
            @RequestParam(value = "birth_date", required = false)
            String birthDate
    ) throws CitizenNotFoundException {
        List<Citizen> citizens = citizenRepo.findCitizens(firstName, lastName, middleName, birthDate);
        if (citizens.isEmpty()){
            log.info("/citizens/{first_name, last_name, middle_name, birth_date} called");
            log.info("first_name = " + firstName);
            log.info("last_name = " + lastName);
            log.info("middle_name = " + middleName);
            log.info("birth_date = " + birthDate);
            throw new CitizenNotFoundException("Citizens with these parametrs not found");
        }
        return new ResponseEntity<>(citizens, HttpStatus.OK);
    }

    @GetMapping(path = "/citizens/{id}")
    @ApiOperation(value = "Возвращает информацию о гражданине по его id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Поиск прошёл успешно"),
            @ApiResponse(code = 400, message = "Ошибка запроса"),
            @ApiResponse(code = 404, message = "Гражданин не найден"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервиса")
    }
    )
    public ResponseEntity<Citizen> getCitizenById(
            @ApiParam(name = "id", value = "id гражданина", example = "1", required = true)
            @RequestParam @Min(1)
            long id
    ) throws CitizenNotFoundException {
        log.info("citizens/{id} calls");
        log.info("id = " + id);
        return new ResponseEntity<>(citizenRepo.findById(id).orElseThrow(() -> new CitizenNotFoundException("Citizen with id " + id + " not found")), HttpStatus.OK);
    }

    @PostMapping("/citizens")
    @ApiOperation(value = "Добавляет в базу нового гражданина")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Гражданин добавлен успешно"),
            @ApiResponse(code = 400, message = "Ошибка запроса"),
            @ApiResponse(code = 422, message = "Ошибка чтения тела запроса"),
            @ApiResponse(code = 500, message = "Внутренняя ошибка сервиса")
    }
    )
    public ResponseEntity<UserPostResponse> postCitizen(
            @ApiParam(name = "first_name", value = "Имя", example = "Михаил", required = true)
            @RequestParam("first_name")
            String firstName,
            @ApiParam(name = "last_name", value = "Фамилия", example = "Горшенёв", required = true)
            @RequestParam("last_name")
            String lastName,
            @ApiParam(name = "middle_name", value = "Отчество", example = "Юрьевич")
            @RequestParam(value = "middle_name", required = false)
            String middleName,
            @ApiParam(name = "birth_date", value = "Дата рождения в формате dd.MM.yyyy", example = "19.06.2013", required = true)
            @RequestParam("birth_date")
            String birthDate,
            @ApiParam(name = "phone", value = "Номер телефона в формате +7(ХХХ)ХХХ-ХХ-ХХ", example = "+7(123)456-78-90", required = true)
            @RequestParam("phone")
            String phone,
            @ApiParam(name = "extra_phone", value = "Дополнительный номер телефона в формате +7(ХХХ)ХХХ-ХХ-ХХ", example = "+7(098)765-43-21")
            @RequestParam(value = "extra_phone", required = false)
            String extraPhone,
            @ApiParam(name = "dul_serie", value = "Серия паспорта (4 цифры)", example = "1111")
            @RequestParam(value = "dul_serie", required = false)
            String dulSerie,
            @ApiParam(name = "dul_number", value = "Номер паспорта (6 цифр)", example = "111111")
            @RequestParam(value = "dul_number", required = false)
            String dulNumber
    ) {
        Citizen citizen = Citizen.builder()
                .firstName(firstName)
                .lastName(lastName)
                .middleName(middleName)
//                .birthDate(birthDate)
                .phone(phone)
                .extraPhone(extraPhone)
                .dulSerie(dulSerie)
                .dulNumber(dulNumber)
                .build();
        citizen.setBirthDate(birthDate);
        System.out.println(citizen);
        citizenRepo.save(citizen);
        UserPostResponse userPostResponse = new UserPostResponse(citizen.getId());
        return new ResponseEntity<>(userPostResponse, HttpStatus.CREATED);
    }
}
