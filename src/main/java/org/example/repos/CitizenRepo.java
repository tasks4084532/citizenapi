package org.example.repos;

import org.example.entity.Citizen;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CitizenRepo extends CrudRepository<Citizen, Long> {

    @Query(value = "SELECT * FROM citizens citizen WHERE " +
            "(:first_name is null OR citizen.first_name = cast(:first_name AS text)) and " +
            "(:last_name is null OR citizen.last_name = cast(:last_name AS text)) and " +
            "(:middle_name is null OR citizen.middle_name = cast(:middle_name AS text)) and " +
            "(:birth_date is null OR to_char(citizen.birth_date, 'DD.MM.YYYY') = cast(:birth_date as text))", nativeQuery = true)
    List<Citizen> findCitizens(@Param("first_name") String firstName,
                               @Param("last_name") String lastName,
                               @Param("middle_name") String middleName,
                               @Param("birth_date") String birthDate);
}