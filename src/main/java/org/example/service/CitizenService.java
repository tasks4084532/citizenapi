package org.example.service;

import org.springframework.stereotype.Service;

@Service
public class CitizenService {
    public String helloWorld(int number) {
        return "Hello, World!\nYour number = " + number;
    }
}
