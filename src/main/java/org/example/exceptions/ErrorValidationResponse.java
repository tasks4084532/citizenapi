package org.example.exceptions;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@ApiModel(description = "Сообщение об ошибке валидации")
@Setter
public class ErrorValidationResponse {

    @ApiModelProperty(value = "Сообщение об ошибке для пользователя",
            example = "Value of id must be more than 0")
    protected String message;

    public ErrorValidationResponse(final String message) {
        this.message = message;
    }
}