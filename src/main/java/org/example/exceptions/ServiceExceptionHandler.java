package org.example.exceptions;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.validation.ConstraintViolationException;

@Slf4j
@ControllerAdvice
public class ServiceExceptionHandler {

    @ExceptionHandler({ConstraintViolationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorValidationResponse handleValidationException(ConstraintViolationException ex) {
        log.warn("Validation exception is handled - Response 400");
        return new ErrorValidationResponse("Value of id must be more than 0");
    }

    @ExceptionHandler({CitizenNotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public CitizenNotFoundException handleCitizenNotFoundException(CitizenNotFoundException ex){
        log.warn("Citizen not found exception handled - Response 404");
        return new CitizenNotFoundException("Citizen with this id not found");
    }
}