package org.example.exceptions;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@ApiModel(description = "Сообщение об отсутствии пользователя с данным id")
@Setter
public class CitizenNotFoundException extends Exception {

    @ApiModelProperty(value = "Сообщение об ошибке для пользователя")
    protected String message;

    public CitizenNotFoundException(final String message) {
        this.message = message;
    }
}