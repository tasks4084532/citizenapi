package org.example.responses;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@Builder
@ApiModel(description = "Сообщение об успешно добавленном пользователе")
@AllArgsConstructor
public class UserPostResponse {
    @ApiModelProperty(value = "id пользователя", example = "1")
    public long new_citizen_id;
}
